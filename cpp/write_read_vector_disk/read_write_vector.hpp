#pragma once

#include <cstdint>
#include <vector>

#include <string>

bool write_to_disk(std::vector<uint8_t> data, std::string file_path);

std::vector<uint8_t> read_from_disk(std::string file_path); 
