#include "read_write_vector.hpp"

#include <fstream>
#include <iterator>


// Function based on: https://stackoverflow.com/a/6406411
// Can be adapted to pure Template based
bool write_to_disk(std::vector<uint8_t> data, std::string file_path)
{
  std::ofstream outstream(file_path, std::ios::out | std::ios::binary);
  std::ostream_iterator<uint8_t> out_iterator(outstream);
  std::copy(data.begin(), data.end(), out_iterator);
  
  return true; 
}

std::vector<uint8_t> read_from_disk(std::string file_path)
{


  std::ifstream instream(file_path, std::ios::in | std::ios::binary);
  std::vector<uint8_t> data((std::istreambuf_iterator<char>(instream)), std::istreambuf_iterator<char>());
  return data;
  
}
